import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Bank implements Runnable {

	private String bankName;
	private int bankAmount;
	private final BlockingQueue<RequestLoan> sharedQueue;
	List<Bank> banks = new ArrayList<Bank>();

	public Bank(String bank, int amount, BlockingQueue<Object> sharedQueue) {
		bankName = bank;
		bankAmount = amount;
		this.sharedQueue = new LinkedBlockingQueue<RequestLoan>();
	}

	@Override
	public void run() {
		//System.out.println("Bank threads");
		try {
			while(true) {
				 RequestLoan request = sharedQueue.take();
				 Customer customer = request.getCustomer();
				 synchronized (customer) {
					boolean loanResult = loanRequest(request.getLoanAmount());
					request.setLoanResult(loanResult);
					customer.notify();
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public String getBankName() {
		return bankName;
	}

	public int getBankAmount() {
		return bankAmount;
	}


	public synchronized boolean loanRequest(int amountCustomerRequesting) {

		if(bankAmount >= amountCustomerRequesting) {
			bankAmount = bankAmount - amountCustomerRequesting ;
			return true;
		}
		return false;
	}

	public void placeRequest(RequestLoan loanRequestLoan) {
		sharedQueue.add(loanRequestLoan);
	}

}
