import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Money {

	public static void main(String[] args) {
		File customersFile = new File(System.getProperty("user.dir")+"\\customers.txt");
		File banksFile = new File(System.getProperty("user.dir")+"\\banks.txt");
		
		BlockingQueue<Object>  sharedQueue= new LinkedBlockingQueue<Object>();
		
		System.out.println("** Banks and financial Resources **");
		Scanner bFile = null;
		Thread bankThread;
		List<Bank> banks = new ArrayList<Bank>();
		try {
			bFile = new Scanner(banksFile);
			while(bFile.hasNextLine()) {
			String string = bFile.nextLine();
			string = string.replace(".", "").replace("{", "").replace("}", "");
			String [] strings = string.split(",");
			Bank bankObject = new Bank(strings[0], Integer.parseInt(strings[1]), sharedQueue);
			banks.add(bankObject);
			bankThread = new Thread(bankObject);
			System.out.println(strings[0]+ ": " + Integer.parseInt(strings[1]));
			bankThread.start();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println();
		
		System.out.println("** Customers and Loan Objectives **");
		Scanner cFile = null;
		Thread customerThread;
		List<Thread> customerThreadList = new ArrayList<Thread>();
		List<Customer> customers = new ArrayList<Customer>();
		try {
			cFile = new Scanner(customersFile);
			while(cFile.hasNextLine()) {
			String string = cFile.nextLine();
			string = string.replace(".", "").replace("{", "").replace("}", "");
			String [] strings = string.split(",");
			Customer customer = new Customer(strings[0], Integer.parseInt(strings[1]), sharedQueue, banks);
			customers.add(customer);
			customerThread = new Thread(customer);
			
			customerThreadList.add(customerThread);
			System.out.println(strings[0]+ ": " + Integer.parseInt(strings[1]));
			customerThread.start();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		
		while(!customerThreadList.isEmpty()) {
			Thread thread = customerThreadList.get(0);
			if(!thread.isAlive())
				customerThreadList.remove(0);
		}
		
		for(int i=0; i<customers.size(); i++) {
			Customer cdCustomer = customers.get(i);
			if(cdCustomer.getCustomerAmount() == 0)
				System.out.println(cdCustomer.getCustomerName() +" has reached the objective of "+ cdCustomer.getInitialAmount() +" dollar(s). Woo Hoo!");
			else {
				System.out.println(cdCustomer.getCustomerName() +" was only able to borrow "+ (cdCustomer.getInitialAmount() - cdCustomer.getCustomerAmount()) +" dollar(s). Boo Hoo!");				
			}
		}
		
		System.out.println();
		
		for (Bank bankDetails : banks) {
			System.out.println(bankDetails.getBankName() +" has " + bankDetails.getBankAmount() +" dollar(s) remaining");
		}
		
		System.exit(0);
	
	}
		

}
