
public class RequestLoan {
	Customer customer;
	int loanAmount;
	boolean loanResult;
	
	public RequestLoan(Customer customer, int loanAmount) {
		this.customer = customer;
		this.loanAmount = loanAmount;
		loanResult = false;
	}

	public boolean isLoanResult() {
		return loanResult;
	}
	
	public void setLoanResult(boolean loanResult) {
		this.loanResult = loanResult;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	
	public int getLoanAmount() {
		return loanAmount;
	}
}
