-module(money).
-import(lists,[foreach/2]).
-export([start/0]).

start() ->
	BankFile = "banks.txt",
	{ok, Banks} = file:consult(BankFile),
	CustomerFile = "customers.txt",
	{ok, Customers} = file:consult(CustomerFile),
	print(Banks, Customers),
	process_creation(Banks, Customers, []),
	master().
 	
print(Banks, Customers) ->
	PrintValues = fun(Value) ->
			Name = element(1, Value),
			Amount = element(2, Value),
			io:fwrite("~p:~w~n",[Name, Amount])
		end,
	io:fwrite("** Customers and Loan Objectives **~n"),
	lists:foreach(PrintValues, Customers),
	io:fwrite("** Banks and financial Resources **~n"),
	lists:foreach(PrintValues, Banks).
	
process_creation([Banks | Tail], Customers, Pids) ->
	Pid = spawn(bank, bank_process, [self(), Banks]),
	Name = element(1, Banks),
	register(Name, Pid),
	NewPids = lists:append(Pids,[Pid]),
	process_creation(Tail, Customers, NewPids);
process_creation([], Customers, BankPids) -> 
	process_customers(Customers, BankPids).

process_customers([Customers | Tail], BankPids) ->
	InitialAmount = element(2, Customers),
	Pid = spawn(customer, customer_process, [self(), Customers, BankPids, InitialAmount]),
	Name = element(1, Customers),
	register(Name, Pid),
	process_customers(Tail, BankPids);
process_customers([], BankPids) -> BankPids.

master() ->
	receive
		{requests, CustomerName, RequestedAmount, BankName}->
			io:fwrite("~p requests a loan of ~p dollar(s) from ~p~n",[CustomerName, RequestedAmount, BankName]),
			master();
		{approves, BankName , RequestedAmount, CustomerName}->
			io:fwrite("~p approves a loan of ~p dollar(s) from ~p~n",[BankName, RequestedAmount, CustomerName]),
			master();
		{denies, BankName , RequestedAmount, CustomerName}->
			io:fwrite("~p denies a loan of ~p dollar(s) from ~p~n",[BankName, RequestedAmount, CustomerName]),
			master();
		{print, CustomerName, CustomerAmount, InitialAmount}->
			if
				CustomerAmount == 0 ->
				io:fwrite("~p has reached the objective of ~p dollar(s). Woo Hoo!~n",[CustomerName, CustomerAmount]);
			true ->
				FinalAmount = InitialAmount - CustomerAmount,
				io:fwrite("~p was only able to borrow ~p dollar(s). Boo Hoo!~n",[CustomerName, FinalAmount])
			end,
			master();
		{printbank, BankName , BankAmount}->
			io:fwrite("~p has ~p dollar(s) remaining.~n",[BankName, BankAmount]),
			master()
	after
		1500 -> ok
	end.