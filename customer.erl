-module(customer).
-export([customer_process/4]).
-import(lists, [nth/2]).

customer_process(MasterProcess, Customers, BankPids, InitialAmount) ->
	SleepTime = rand:uniform(90),
	ThreadSleepTime = SleepTime + 10,
	timer:sleep(ThreadSleepTime),
	Len = length(BankPids),
	CustomerName = element(1, Customers),
	CustomerAmount = element(2, Customers),
	Length = Len /= 0,
	Amount = CustomerAmount /= 0,
	if
		Length and Amount ->
			Number_random = rand:uniform(Len),
			Bankid = nth(Number_random, BankPids),
			{_, BankName} = erlang:process_info(Bankid, registered_name),
			if
				CustomerAmount >= 50 ->
					Value = 50;
			true ->
					Value = CustomerAmount
			end,
			RequestedAmount = rand:uniform(Value),
			
			MasterProcess ! {requests, CustomerName, RequestedAmount, BankName},
			Bankid ! {self(), requests, RequestedAmount, CustomerName},
			receive
				{rejected, BankName}->
					BankPid = lists:delete(Bankid, BankPids),
					customer_process(MasterProcess, Customers, BankPid, InitialAmount);
				{approved, RequestedAmount}->
					RemainingAmount = CustomerAmount - RequestedAmount,
					customer_process(MasterProcess, {CustomerName, RemainingAmount}, BankPids, InitialAmount)
			after
				3000 -> MasterProcess ! {print, CustomerName, CustomerAmount, InitialAmount}
			end;
	true ->
		MasterProcess ! {print, CustomerName, CustomerAmount, InitialAmount}
	end.
	
	
	