import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;

public class Customer implements Runnable {

	String customerName;
	int customerAmount;
	int initialAmount;
	private final BlockingQueue<Object> sharedQueue;
	HashMap<String, List<String>> rejectedData = new HashMap<String, List<String>>();
	List<String> bankRejectedList = new ArrayList<String>();
	List<Bank> bankList = new ArrayList<Bank>();

	public Customer(String customer, int amount, BlockingQueue<Object> sharedQueue, List<Bank> banks) {
		customerName = customer;
		customerAmount = amount;
		initialAmount = amount;
		this.sharedQueue = sharedQueue;
		bankList.addAll(banks);
	}


	@Override
	public void run() {
		Random random = new Random();
		try {
			Thread.sleep(random.nextInt(90)+11);
			while (customerAmount != 0  && (rejectedData.isEmpty() || rejectedData.get(customerName).size() != bankList.size())) {
				synchronized (this) {
					Bank bDetails = bankList.get(random.nextInt(bankList.size()));
					if(rejectedData.isEmpty() || !rejectedData.get(customerName).contains(bDetails.getBankName())) {
						int amountCustomerRequesting = randomCustomerAmount(random);
						System.out.println(customerName + " requests a loan of " + amountCustomerRequesting +" dollar(s) from "+bDetails.getBankName());
						Thread.sleep(random.nextInt(90)+10);
						RequestLoan loanRequestLoan = new RequestLoan(this, amountCustomerRequesting);
						bDetails.placeRequest(loanRequestLoan);
						this.wait();
						boolean loanResult = loanRequestLoan.isLoanResult();
						if(loanResult) {
							customerAmount = customerAmount - amountCustomerRequesting;
							System.out.println(bDetails.getBankName()+ " approves a loan of " + amountCustomerRequesting +" dollar(s) from "+customerName);
						}else {
							if(!bankRejectedList.contains(bDetails.getBankName())) {
								bankRejectedList.add(bDetails.getBankName());
								rejectedData.put(customerName, bankRejectedList);
								System.out.println(bDetails.getBankName()+ " denies a loan of " + amountCustomerRequesting +" dollar(s) from "+customerName);						
							}
						}
					}
				}

			}
		} catch (InterruptedException e) {
			System.out.println("Customer waiting/sleeping a random period interrupted");
		}

	}

	public int randomCustomerAmount(Random random) {
		int amountLimit = customerAmount >= 50 ? 50 : customerAmount;
		return random.nextInt(amountLimit) + 1;
	}

	public String getCustomerName() {
		return customerName;
	}

	public int getCustomerAmount() {
		return customerAmount;
	}

	public int getInitialAmount() {
		return initialAmount;
	}

}
