-module(bank).
-export([bank_process/2]).

bank_process(MasterProcess, Banks)->
	BankName = element(1, Banks),
	BankAmount = element(2, Banks),
	receive
		{CustomerProcess, requests, RequestedAmount, CustomerName} ->
			if
				BankAmount >= RequestedAmount ->
					RemainedAmount = BankAmount - RequestedAmount,
					MasterProcess ! {approves, BankName , RequestedAmount, CustomerName},
					CustomerProcess ! {approved, RequestedAmount},
					bank_process(MasterProcess, {BankName, RemainedAmount});
			true ->
				MasterProcess ! {denies, BankName , RequestedAmount, CustomerName},
				CustomerProcess ! {rejected, BankName},
				bank_process(MasterProcess, Banks)
			end
	after
		1500 -> MasterProcess ! {printbank, BankName , BankAmount}
	end.
	